import Grade from '@/components/Grade.vue'
import Course from '@/components/Course.vue'

const routes = [
  { path: '/', component: Grade, name: 'grade' },
  { path: '/cursos', component: Course, name: ' course' }
]

export default routes
